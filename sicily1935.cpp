// 题目分析:
//对于知道前序和中序建树,从前序开始向后,在中序中字符找到相应的位置,
//然后中序中的左右两边组成该字符的.左右子树；
//循环这个规律，直到前序都遍历了一次。从而建立二叉树.

// 题目网址:http://soj.me/1935

#include<iostream>
#include<string>
#include<queue>
using namespace std;
struct Node
{
    char ch;
    Node* left;
    Node* right;
};
Node *root;
string s1, s2;
int position;
queue<Node*> que;
int findPos(char ch)
{
    int i;
    for(i = 0; i < s2.size(); i++)
        if(s2[i] == ch)
            break;
    return i;
}
void buildtree(Node* &sub_root,int i, int j)
{
    if(i > j) sub_root = NULL;
    else{
        sub_root = new Node;
        int k = findPos(s1[position++]);
        sub_root->ch = s2[k];
        buildtree(sub_root->left, i, k-1);
        buildtree(sub_root->right, k+1, j);
    }
}
void BFS()
{
    que.push(root);
    while(!que.empty()){
        Node *nd;
        nd = que.front();
        cout << nd->ch;
        if(nd->left != NULL)
            que.push(nd->left);
        if(nd->right != NULL)
            que.push(nd->right);
        que.pop();
    }
    cout << endl;
}
int main()
{
    int t;
    cin >> t;
    while(t--){
        cin >> s1 >> s2;
        position = 0;
        buildtree(root,0,s1.size()-1);
        BFS();
    }
    return 0;
}                                 